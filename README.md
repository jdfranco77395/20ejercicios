# **Ejercicios Javascript**

_ En este proyecto realizaremos 20 diferentes ejercicios con el fin de tener un mejor aprendizaje sobre la utilización del lenguaje javascript

## **Ejercicios:** 📋

#### Ejercicio1: 
_Este ejercicio permite que el usuario ingrese el nombre para que aparezca en pantalla "ahora estas en la matrix, (nombre)"
#### Ejercicio2: 
_ En este ejercicio permite la suma de 2 números, uno decimal y un entero. El resultado será mostrado en un campo llamado "el resultado de la suma es (resultado)" 
#### Ejercicio3: 
_ En este ejercicio permite la suma de 2 números, El resultado de la suma de estos 2 números , se solicita al usuario un 3 numero el cual será multiplicado por el resultado de la suma, mostrando en pantalla los resultados tanto de la suma como de la multiplicación
#### Ejercicio4: 
_ En este ejercicio el usurario deberá escribir la cantidad de kilómetros recorridos por una motocicleta y la cantidad de litros de combustible consumidos durante este recorrido, y así poder mostrar el consumo de combustible por kilometro
#### Ejercicio5: 
_ En este ejercicio permite que el usuario ingrese la temperatura en Fahrenheit (numero decimal) y convertirlo a grados Celsius
#### Ejercicio6: 
_ En este ejercicio permite mostrar el promedio de 3 números ingresados por el usuario
#### Ejercicio7: 
_ En este ejercicio permite descontar a un numero ingresado por el usuario el 15% de descuento 
#### Ejercicio8: 
_ En este ejercicio solicita al usuario 2 palabras, las cuales serán concatenadas con un espacio en medio de las 2 palabras
#### Ejercicio9: 
_ En este ejercicio se muestra la ubicación de un carácter
#### Ejercicio10: 
_ En este ejercicio el usuario podrá ingresar a cuantos shows musicales ha asistido durante el ultimo año, si el usuario a visto mas de 3 shows musicales deberá retornar TRUE de lo contrario retornará FALSE
#### Ejercicio11: 
_ En este ejercicio el usuario deberá ingresar una fecha formada por 8 números, donde representan el día, mes y año (DDMMAAAA) y mostrara al usuario en formato fecha DD/MM/AAAA
#### Ejercicio12: 
_ En este ejercicio solicitara al usuario ingresar un numero entero, si el numero es un numero par se debe retornar TRUE de lo contrario FALSE
#### Ejercicio13: 
_ En este ejercicio solicitara al usuario ingresar la edad, la cantidad de artículos comprados. si el usuario es mayor a 18 años y compro mas de un articulo el sistema deberá retornar TRUE
#### Ejercicio14: 
_ En este ejercicio a partir de una cadena de texto ingresada por el usuario, se deben contar los caracteres y si la cuenta da un numero par se debe retornar TRUE

#### Ejercicio15
_ Escribí un programa que le pida al usuario ingresar dos palabras y las guarde en dos variables, y que luego imprima TRUE si la primera palabra es menor que la segunda o False si no lo es 

#### Ejercicio16
_ Escribí un programa para pedir al usuaro su nombre y luego el nombre de la persona, almacenando cada nombre en una variable.luego mostrar en pantalla un valor de verdad que indique si: los nombres de ambas personas comienzan con la misma letra o si terminan con la misma letra. por ejemplo, si los nombres ingresador son Maria y marcos, se mostrara TRUE, ya que comienzan con la misma letra, o si pedro y diego, TRUE ya que finalizan en la misma letra de lo contrario FALSE


#### Ejercicio17
_ Escribí un programa para pedir al usuaro su nombre y luego el nombre de la persona, almacenando cada nombre en una variable.luego mostrar en pantalla un valor de verdad que indique si: los nombres de ambas personas comienzan con la misma letra o si terminan con la misma letra. por ejemplo, si los nombres ingresador son Maria y marcos, se mostrara TRUE, ya que comienzan con la misma letra, o si pedro y diego, TRUE ya que finalizan en la misma letra de lo contrario FALSE

#### Ejercicio18
_ Escribí un programa  que solicite al usuario el ingreso de dos numero diferentes y muestre en pantalla al mayor de los dos 

#### Ejercicio19
_ Escribí un programa  que solicite al usuario una letra y, si es vocal, muestre el mensaje "Es vocal" verificar si el usuario ingreso un strin de mas de un caracter, informar que es erroneo 

#### Ejercicio20
_ Escribí un programa para solicitar al usuario tres numero y mostrar en pantalla al menor de los tres



## Autores ✒️
#### - Juan Diego Franco Montoya
#### - Maria Camila Herrera Muñoz